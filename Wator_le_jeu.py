########################################
# Wator : simulation proies,prédateurs #
########################################

from random import randint # importe du module random la fonction randint
import matplotlib.pyplot as plt
import numpy as np

def nbre_aleatoire(min,max) :
    '''
    tire un nombre entier aléatoire compris entre les valeurs min et max
    : min : (int) plus petite valeur possible
    : max : (int) plus grande valeur possible
    : return : (int) un nombre entier aléatoire compris entre les valeurs min et max
    
    >>> 
    '''
    return randint(min,max)

def remplir_la_mer(largeur,hauteur) :
    '''
    Retourne une liste de n listes ( n : hauteur ) composée chacune de k zéros ( k : largeur )
    : largeur : (int) largeur de la mer
    : hauteur : (int) hauteur de la mer
    : effets de bords : remplit la liste de 0
    : 
    
    >>>remplir_la_mer(mer,3,2)
    [[0, 0, 0], [0, 0, 0]]
    '''
    mer=[[0]*largeur for i in range(hauteur)]# crée une liste remplie de nombres 0 , et de largeur le nombre largeur
    return mer
    
def ajout_thon(mer,population_thons,x,y,gestation) :
    '''
    ajoute un thon à la liste représentant la mer et à la liste de la population des thons
    : mer : (list) liste à 2 dimensions représentant la mer
    : population_thon : (list) liste représentant les thons et leur propriétés
    : x : (int) position horizontale du thon
    : y : (int) position verticale du thon
    : gestation : (int) durée de gestation du thon
    : return : aucun
    : effets de bords : modifie les listes mer et population_thons
    '''
    mer[x][y]="thon"
    population_thon.append([x,y,gestation])
    
    return

def case_vide(mer,x,y):
    '''
    déterminer si un poisson est présent à la position x,y
    : mer : (list) liste représentant l'état de la mer
    : x : (int) abscisse de la case
    : y : (int) ordonnée de la case
    : effets de bords : aucun
    : return : (booleen) True si la case est vide, False sinon.
    '''
    if mer[y][x]==0 :
        return True
    else :
        return False

def creation_population_thons(mer,nbre_thons,population_thons,GESTATION_THON):
    '''
    Remplit la mer du nombre de thons désirés
    : mer : (list)
    : nbre_thons : (int)
    : population_thons : (list) contient
    : Effets de bords : modifie les listes mer et population_thons
    '''
    hauteur=len(mer)
    largeur=len(mer[0])
    for i in range(0,nbre_thons):
        x=nbre_aleatoire(0,largeur-1)
        y=nbre_aleatoire(0,hauteur-1)
        while case_vide(mer,x,y)!=True :
            x=nbre_aleatoire(0,largeur-1)
            y=nbre_aleatoire(0,hauteur-1)
        mer[y][x]="T" # on place T pour signaler que la case est occupée par un thon
        population_thons.append([x,y,GESTATION_THON]) # on ajoute la liste à la liste décrivant les thons
    return mer,population_thons
            
def cases_libres_aux_alentours(mer,x,y):
    '''
    détermine les cases libres autour d'une case de coordonnées x,y
    : mer : (list) plateau représentant la mer
    : x : (int) coordonnée horizontale de la case
    : y : (int) coordonné verticale de la case
    : return : (list) une liste contenant les coordonnées des cases libres aux alentours
    '''
    cases_libres=[] # on définit une liste contenant les éventuelles cases vides autours de la case (x,y)
    x_voisins=[] # listes contenant les abscisses des cases adjacentes
    y_voisins=[] # listes contenant les ordonnées des cases adjacentes
    # on tient conpte de la géométrie théorique de la mer
    if x==0 : # on détermine les coordonnées de la case adjacente à gauche
        x_voisins.append(len(mer[0])-1)
    else :
        x_voisins.append(x-1)
    y_voisins.append(y)
    if y==0 :  # on détermine les coordonnées de la case supérieure
        y_voisins.append(len(mer)-1)
    else :
        y_voisins.append(y-1)
    x_voisins.append(x)
    if x==len(mer[0])-1 : # on détermine les coordonnées de la case adjacente à droite
        x_voisins.append(0)
    else :
        x_voisins.append(x+1)
    y_voisins.append(y)
    if y==len(mer)-1 : # on détermine les coordonnées de la case inférieure
        y_voisins.append(0)
    else :
        y_voisins.append(y+1)
    x_voisins.append(x)
    # on détermine quelles sont les cases adjacentes vides
    for i in range(0,4) :
        if case_vide(mer,x_voisins[i],y_voisins[i])== True :
            cases_libres.append([x_voisins[i],y_voisins[i]])
    return cases_libres

def remise_a_jour_population_thons(mer,population_thons):
    '''
    mise en place des thons dans la liste mer
    : mer : (list)
    : population_thons : (list)
    : return : la liste mer modifiée
    '''
    # on remplit la liste mer de 0
    mer = remplir_la_mer(len(mer[0]),len(mer))
    # On ajoute les thons 
    for thon in population_thons :
        mer[thon[1]][thon[0]]="T"
    return mer
    



def deplacement_thons_et_ajout_thons(mer,population_thons,GESTATION_THON) :
    '''
    déplacement aléatoire des thons et naissances éventuelles
    : mer : (list)
    : population_thons : (list)
    : GESTATION_THON : (int) constante entière
    '''
    population_nouveaux_thons=[] # liste contenant les nouveaux thons
    # on déplace chaque thon
    for thon in population_thons :
        x=thon[0] # coordonnée horizontale du thon
        y=thon[1] # coordonnée verticale du thon
        cases_libres=cases_libres_aux_alentours(mer,x,y) # On déterminer la liste des cases vides aux alentours
        if not(len(cases_libres)==0) : # déplacement du thon si une case est libre
            choix_aleatoire=nbre_aleatoire(0,len(cases_libres)-1) # on choisit aléatoirement la nouvelle position
            thon[0]=cases_libres[choix_aleatoire][0] # on mémorise les nouvelles coordonnées
            thon[1]=cases_libres[choix_aleatoire][1]
            mer[y][x]=0
            mer[thon[1]][thon[0]]='T'
            if thon[2]==0 : # création d'un nouveau thon si la période de gestation est à 0
                population_nouveaux_thons.append([x,y,GESTATION_THON])
                thon[2]=GESTATION_THON # on remet la période à la valeur initiale
                mer[y][x]='T'
        thon[2]=thon[2]-1 # on diminue le temps de gestation
    # On ajoute les nouveaux thons à la population des thons
    for thon in population_nouveaux_thons :
        population_thons.append(thon)
    # On remet à jour les positions des poissons dans la mer
    mer=remise_a_jour_population_thons(mer,population_thons)
    return mer,population_thons
    
def affichage_evolution(evolution_mer,hauteur,largeur) :
    '''
    affiche l"état de la mer à chaque évolution
    : evolution mer : (list) liste contenant les évolutions successives
    '''
    for mer in evolution_mer :
        print(mer)
        #for i in range(0,len(mer)):
           # print(mer[i])
       # print("")
       # print("etape suivante")
        map=np.random.randn(hauteur, largeur)
        for y in range(0,len(mer)):
            for x in range(0,len(mer[0])):
                if mer[y][x]==0 :
                    map[y,x]=0
                if mer[y][x]=="T" :
                    map[y,x]=1
        

        plt.spy(map)
        #ax2.spy(map, precision=0.1, markersize=5)

        #ax3.spy(map)
        #ax4.spy(map, precision=0.1)

        plt.show()
    
    
    
def essai_graphe():
    x = [ 1, 3, 2, 1]
    y = [ 2, 3, 1, 3]
    plt.scatter(x,y)
    plt.show()
        
        
                                     
    
    


def simulation(largeur,hauteur,nbre_thons,nbre_etapes):
    '''
    lancement de la simulation
    : largeur : (int) largeur de la mer
    : hauteur : (int) hauteur de la mer
    : nombre_thons : (int)
    : nbre_etapes : (int) nombre d'étapes de la simulation
    : return : affichage de l'évolution
    : effets de bords : affichage,modification des listes
    '''
    GESTATION_THON=3
    evolution_mer=[] # liste contenant les évolutions successives de la mer
    population_thons=[] # liste qui contiendra les propriétés des poissons [x,y,GESTATION_THON]
    mer=remplir_la_mer(largeur,hauteur) # on remplit la mer de 0
    mer,population_thons = creation_population_thons(mer,nbre_thons,population_thons,GESTATION_THON) # on place les thons aux hasard
    # Simulation de l'évolution de la population de thons étape par étape
    for i in range(0,nbre_etapes):
        mer,population_thons=deplacement_thons_et_ajout_thons(mer,population_thons,GESTATION_THON) # la population de thons se déplace et s'accroit éventuellement
        evolution_mer.append(mer) # on mémorise l'état de la mer
    affichage_evolution(evolution_mer,hauteur,largeur)
        
        
        
    
    
    
    
    
    
        
        
        
        
    
    